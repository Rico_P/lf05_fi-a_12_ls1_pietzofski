/**
  *   Aufgabe:  Recherechieren Sie im Internet !
  * 
  *   Sie d�rfen nicht die Namen der Variablen ver�ndern !!!
  *
  *   Vergessen Sie nicht den richtigen Datentyp !!
  *
  *
  * @version 1.0 from 21.08.2019
  * @author Rico Pietzofski
  */

public class WeltDerZahlen {

  public static void main(String[] args) {
    
    /*  *********************************************************
    
         Zuerst werden die Variablen mit den Werten festgelegt!
    
    *********************************************************** */
    // Im Internet gefunden ?
    // Die Anzahl der Planeten in unserem Sonnesystem                    
    byte anzahlPlaneten =  8;
    
    // Anzahl der Sterne in unserer Milchstra�e
    long anzahlSterne = 300000000000l;
    
    // Wie viele Einwohner hat Berlin?
    int bewohnerBerlin = 3766082;
    
    // Wie alt bist du?  Wie viele Tage sind das?
    
    int alterTage = 365*22; // Oder direkt als 8030 Tage
    
    // Wie viel wiegt das schwerste Tier der Welt?
    // Schreiben Sie das Gewicht in Kilogramm auf!
    int gewichtKilogramm = 150000;   
    
    // Schreiben Sie auf, wie viele km� das gr��te Land er Erde hat?
    int flaecheGroessteLand = 17130000;
    
    // Wie gro� ist das kleinste Land der Erde?
    
    double flaecheKleinsteLand = 0.44;
    
    
    
    
    /*  *********************************************************
    
         Programmieren Sie jetzt die Ausgaben der Werte in den Variablen
    
    *********************************************************** */
    
    System.out.println("Anzhahl der Planeten: " + anzahlPlaneten);
    System.out.println("Anzahl der Sterne: " + anzahlSterne);
    System.out.println("Bewohner Berlin: " + bewohnerBerlin);
    System.out.println("Alter in Tagen: " + alterTage);
    System.out.println("Gewicht des schwersten Tieres: " + gewichtKilogramm);
    System.out.println("Fl�che des gr��ten Landes: " + flaecheGroessteLand);
    System.out.println("Fl�che des kleinsten Landes: " + flaecheKleinsteLand);
    System.out.println(); // Leere Zeile f�r Formatierung
    
    
    System.out.println(" *******  Ende des Programms  ******* ");
    
  }
}
