import java.util.Scanner;

public class PCHaendler {
	
	private static final Scanner Scanner = new Scanner(System.in); // Scanner in Konstante

	public static void main(String[] args) {

		// Benutzereingaben lesen
		String artikel = liesString("was m�chten Sie bestellen?");
		int anzahl = liesInt("Geben Sie die Anzahl ein:");
		double preis = liesDouble("Geben Sie den Nettopreis ein:");
		double mwst = liesDouble("Geben Sie den Mehrwertsteuersatz in Prozent ein: ");

		// Verarbeiten
		double nettogesamtpreis = berechneGesamtnettopreis(anzahl,preis);
		double bruttogesamtpreis = berechneGesamtbruttopreis(nettogesamtpreis, mwst);

		// Ausgeben

		rechungausgeben(artikel, anzahl, nettogesamtpreis, bruttogesamtpreis, mwst);

	}
	
	public static String liesString(String text) {
		System.out.println(text);
		String artikel = Scanner.next();
		return artikel;
	}
	
	public static int liesInt(String text) {
		System.out.println(text);
		int anzahl = Scanner.nextInt();
		return anzahl;
	}
	
	public static double liesDouble(String text) {
		System.out.println(text);
		double wert = Scanner.nextDouble();
		return wert;
	}

	public static double berechneGesamtnettopreis(int anzahl, double nettopreis) {
		double nettoGesamtpreis = anzahl * nettopreis;
		return nettoGesamtpreis;
	}
	
	public static double berechneGesamtbruttopreis(double nettogesamtpreis,double mwst) {
		double bruttogesamtpreis = nettogesamtpreis * (1 + mwst / 100);
		return bruttogesamtpreis;
	}
	
	public static void rechungausgeben(String artikel, int anzahl, double nettogesamtpreis, double bruttogesamtpreis, double mwst) {
		System.out.println("\tRechnung");
		System.out.printf("\t\t Netto:  %-20s %6d %10.2f %n", artikel, anzahl, nettogesamtpreis);
		System.out.printf("\t\t Brutto: %-20s %6d %10.2f (%.1f%s)%n", artikel, anzahl, bruttogesamtpreis, mwst, "%");
	}



}