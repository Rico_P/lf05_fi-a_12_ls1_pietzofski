import java.util.Scanner;
public class Uebung {
	
	//private static final Scanner scanner = new Scanner(System.in); // Scanner in Konstante
	
	public static void main (String[] args) {
		// Aufgabe 1: Mittelwert
		double x = liesDouble("Gebe Sie den ersten Wert ein: ");
		double y = liesDouble("Gebe Sie den zweiten Wert ein: ");
		double m = berechneMittelwert (x,y);
		
		
		mittelwertAusgeben(m,x,y);
		
	}
	
	public static double berechneMittelwert (double x, double y) {
		double m = (x + y) / 2;
		return m;
	}
	
	public static void mittelwertAusgeben(double m, double wert1, double wert2) {
		System.out.printf("Der Mittelwert von %.2f und %.2f ist %.2f",wert1,wert2,m);
	}
	
	public static double liesDouble (String eingabeText) {
		Scanner myScanner = new Scanner(System.in);
		System.out.println(eingabeText);
		double wert = myScanner.nextDouble();
		// double wert = scanner.nextDouble(); // Altenative mit scanner aus konstante
		return wert;
	}

}
