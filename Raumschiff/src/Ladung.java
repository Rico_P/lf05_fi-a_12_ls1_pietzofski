/**
 * 
 * @author Pietzofski
 *
 */
public class Ladung {

	private String bezeichnung;
	private int menge;

	public Ladung() {

	}
	
	/**
	 * @param bezeichnung Der Name der Ladung
	 * @param menge Wie oft die Ladung vorhanden ist
	 */
	public Ladung (String bezeichnung, int menge) {
		this.bezeichnung = bezeichnung;
		this.menge = menge;
	}

	public String getBezeichnung() {
		return bezeichnung;
	}

	public void setBezeichnung(String bezeichnung) {
		this.bezeichnung = bezeichnung;
	}

	public int getMenge() {
		return menge;
	}

	public void setMenge(int menge) {
		this.menge = menge;
	}
	
	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
	    return "Type: " + this.getBezeichnung() + 
	           ", Menge: " + this.getMenge();
	}
}


