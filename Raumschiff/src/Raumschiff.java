
/**
 * @author Pietzofski
 * @version 1.0
 */
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

public class Raumschiff {

	private int photonentorpedoAnazahl;
	private int energieversorgungInProzent;
	private int schildeInProzent;
	private int huelleInProzent;
	private int lebenserhaltungssystemeInProzent;
	private String schiffname;
	private int androidenAnzahl;
	private ArrayList<String> broadcastKommunikator = new ArrayList<String>();
	private ArrayList<Ladung> ladungsverzeichnis = new ArrayList<Ladung>();

	// Konstruktoren

	public Raumschiff() {

	}

	public Raumschiff(int photonentorpedoAnazahl, int energieversorgungInProzent, int schildeInProzent,
			int huelleInProzent, int lebenserhaltungssystemeInProzent, String schiffname, int androidenAnzahl) {

		this.photonentorpedoAnazahl = photonentorpedoAnazahl;
		this.energieversorgungInProzent = energieversorgungInProzent;
		this.schildeInProzent = schildeInProzent;
		this.huelleInProzent = huelleInProzent;
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
		this.androidenAnzahl = androidenAnzahl;
		this.schiffname = schiffname;
	}

	// Getter und Setter

	public int getPhotonentorpedoAnazahl() {
		return photonentorpedoAnazahl;
	}

	public void setPhotonentorpedoAnazahl(int photonentorpedoAnazahl) {
		this.photonentorpedoAnazahl = photonentorpedoAnazahl;
	}

	public int getEnergieversorgungInProzent() {
		return energieversorgungInProzent;
	}

	public void setEnergieversorgungInProzent(int energieversorgungInProzent) {
		this.energieversorgungInProzent = energieversorgungInProzent;
	}

	public int getSchildeInProzent() {
		return schildeInProzent;
	}

	public void setSchildeInProzent(int schildeInProzent) {
		this.schildeInProzent = schildeInProzent;
	}

	public int getHuelleInProzent() {
		return huelleInProzent;
	}

	public void setHuelleInProzent(int huelleInProzent) {
		this.huelleInProzent = huelleInProzent;
	}

	public int getLebenserhaltungssystemeInProzent() {
		return lebenserhaltungssystemeInProzent;
	}

	public void setLebenserhaltungssystemeInProzent(int lebenserhaltungssystemeInProzent) {
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
	}

	public int getAndroidenAnzahl() {
		return androidenAnzahl;
	}

	public void setAndroidenAnzahl(int androidenAnzahl) {
		this.androidenAnzahl = androidenAnzahl;
	}

	public String getSchiffname() {
		return schiffname;
	}

	public void setSchiffname(String schiffname) {
		this.schiffname = schiffname;
	}

	public ArrayList<String> getBroadcastKommunikator() {
		return broadcastKommunikator;
	}

	public void setBroadcastKommunikator(ArrayList<String> broadcastKommunikator) {
		this.broadcastKommunikator = broadcastKommunikator;
	}

	public ArrayList<Ladung> getLadungsverzeichnis() {
		return ladungsverzeichnis;
	}

	public void setLadungsverzeichnis(ArrayList<Ladung> ladungsverzeichnis) {
		this.ladungsverzeichnis = ladungsverzeichnis;
	}

	// Methoden

	/**
	 * @param neueLadung
	 *            Ladung die dem Ladungsverzeichnis hinzugef�g werden soll
	 */
	public void addLadung(Ladung neueLadung) {
		this.ladungsverzeichnis.add(neueLadung);
	}

	/**
	 * @param r
	 *            Raumschiff welches getroffen wird Abschuss einer Photonentorpedes
	 *            wenn welche Vorhanden sind + Nachricht an Alle
	 */
	public void photonentorpedoSchiessen(Raumschiff r) {
		if (this.photonentorpedoAnazahl < 1) {
			this.nachrichtAnAlle("=*Click*=-");
		} else {
			this.photonentorpedoAnazahl--;
			this.nachrichtAnAlle("Photonentorpedo abgeschossen");
			this.treffer(r);
		}
	}

	/**
	 * @param r
	 *            Raumschiff welches getroffen wird Abschuss der Phaserkanone wenn
	 *            genug Energie vorhanden ist + Nachricht an Alle
	 */
	public void phaserkanoneSchiessen(Raumschiff r) {
		if (this.energieversorgungInProzent < 50) {
			this.nachrichtAnAlle("=*Click*=");
		} else {
			this.energieversorgungInProzent -= 50;
			this.nachrichtAnAlle("Phaserkanone abgeschossen");
			this.treffer(r);
		}
	}

	/**
	 * @param r
	 *            Raumschiff das von einem Schuss getroffen wurde Ausgabe des
	 *            Getroffenen Schiffes + Entsprechende Anpassung der Schile
	 */
	private void treffer(Raumschiff r) {
		System.out.println(r.schiffname + " wurde getroffen!");
		// Treffer vermerken
		r.schildeInProzent -= 50;
		if (r.schildeInProzent <= 0) {
			r.schildeInProzent = 0;
			r.huelleInProzent -= 50;
			r.energieversorgungInProzent -= 50;
		}

		if (r.huelleInProzent <= 0) {
			r.huelleInProzent = 0;
			r.lebenserhaltungssystemeInProzent = 0;
			this.nachrichtAnAlle("Die Lebenserhaltungssysteme von " + r.schiffname + " wurden vernichtet.");
		}
	}

	/**
	 * @param message
	 *            Nachricht die ausgegeben wird und dem Broadcast Kommunikator
	 *            hinzugef�gt wird Nachricht wurd auf der Konsole ausgegeben und dem
	 *            Broadcast Kommunikator hinzugef�gt
	 */
	public void nachrichtAnAlle(String message) {
		System.out.println("\nNachricht an Alle:");
		System.out.println("\t" + message);
		this.broadcastKommunikator.add(message);
	}

	/**
	 * @return Gibt den Broadcast Kommunikator des Raumschiffes zur�ck
	 */
	public ArrayList<String> eintraegeLobguchZurueckgeben() {
		return this.broadcastKommunikator;
	}

	/**
	 * @param logbuch
	 *            Eine ArrayList vom Typ String, das Logbuch eines Raumschiffes
	 *            Formatierte Ausgabe des Logbuches
	 */
	public void logbuchAusgeben(ArrayList<String> logbuch) {
		System.out.printf("\n%s\"%s\":\n", "Logbuch: ", this.schiffname);
		for (String s : logbuch) {
			System.out.println("\t" + s);
		}
	}

	/**
	 * @param anzahlTorpedos
	 *            Die anzahl and Torpedos welche geladen werden soll Nachladen der
	 *            Photonentorpedos wenn keine mehr im Magazin sind �berpr�ft ob in
	 *            der Ladung noch Torpedos zur Verf�gung stehen, wenn ja werden
	 *            diese von dort abgezogen und ins Magazin geladen Gibt auch aus
	 *            wenn keine Torpedos zum Laden vorhanden sind.
	 */
	public void photonentorpedosLaden(int anzahlTorpedos) {
		boolean check = false;

		for (int i = 0; i < this.getLadungsverzeichnis().size(); i++) {
			if (this.getLadungsverzeichnis().get(i).getBezeichnung().toLowerCase().equals("photonentorpedo")) {
				if (this.getLadungsverzeichnis().get(i).getMenge() >= anzahlTorpedos) {
					check = true;
					this.photonentorpedoAnazahl = anzahlTorpedos;
					int neueMenge = this.getLadungsverzeichnis().get(i).getMenge() - anzahlTorpedos;
					this.getLadungsverzeichnis().get(i).setMenge(neueMenge);
					System.out.println(this.photonentorpedoAnazahl + " Photonentorpedo"
							+ (this.photonentorpedoAnazahl > 1 ? "s" : "") + " eingesetzt");
					break;
				}
			}

		}

		if (check) {
		} else {
			System.out.println("Keine Photonentorpedos gefunden!");
			this.nachrichtAnAlle("-=*Click*=-");
		}
	}

	/**
	 * @param schutzschilde
	 *            true wenn Schutzschilde gest�rkt werden soll
	 * @param energieversorgung
	 *            true wenn Energieversorung gest�rkt werden soll
	 * @param schiffshuelle
	 *            true wenn Schiffshuelle gest�rkt werden soll
	 * @param anzahlDroiden
	 *            Anzahl der Doriden die f�r die Reperatur verwendet werden sollen
	 *            Die Methode entscheidet anhand der �bergebenen Parameter, welche
	 *            Schiffsstrukturen repariert werden sollen. Es wird eine
	 *            Zufallszahl zwischen 0 - 100 erzeugt, welche f�r die Berechnung
	 *            der Reparatur ben�tigt wird. Ist die �bergebene Anzahl von
	 *            Androiden gr��er als die vorhandene Anzahl von Androiden im
	 *            Raumschiff, dann wird die vorhandene Anzahl von Androiden
	 *            eingesetzt.
	 */
	public void reparaturDurchfuehren(boolean schutzschilde, boolean energieversorgung, boolean schiffshuelle,
			int anzahlDroiden) {

		int zuReparieren = 0;
		if (schutzschilde)
			zuReparieren++;
		if (energieversorgung)
			zuReparieren++;
		if (schiffshuelle)
			zuReparieren++;

		// Zufallszahl generieren
		Random random = new Random();
		int r = random.nextInt(100 + 1) + 1;

		if (anzahlDroiden > this.androidenAnzahl) {
			anzahlDroiden = this.androidenAnzahl;
		}

		if (zuReparieren != 0) {
			double reparaturInProzent = (r * anzahlDroiden) / zuReparieren;

			if (schutzschilde)
				this.schildeInProzent += reparaturInProzent;
			if (energieversorgung)
				this.energieversorgungInProzent += reparaturInProzent;
			if (schiffshuelle)
				this.huelleInProzent += reparaturInProzent;
		}

	}

	/**
	 * Formatiert Ausgabe aller Parameter eines Raumschiffes
	 */
	public void zustandRaumschiff() {
		System.out.println("\nSchiffsnamen: " + this.schiffname);
		System.out.println("Energieversorung: " + this.energieversorgungInProzent + "%");
		System.out.println("Anzahl Photonentorpedos: " + this.photonentorpedoAnazahl);
		System.out.println("Schild: " + this.schildeInProzent + "%");
		System.out.println("H�lle: " + this.huelleInProzent + "%");
		System.out.println("Lebenserhaltungssysteme: " + this.lebenserhaltungssystemeInProzent + "%");
		System.out.println("Anzahl Androiden: " + this.androidenAnzahl);
	}

	/**
	 * Formatierte Ausgabe des Landungsveerzeichnisses
	 */
	public void ladungsverzeichnisAusgeben() {
		System.out.println("\nLadungsverzeichnis: ");
		for (Ladung ladung : this.getLadungsverzeichnis()) {
			System.out.println("\t" + ladung);
		}
	}

	/**
	 * Entfernt Ladung wenn Menge der Laudung gleich 0 ist
	 */
	public void ladungsverzeichnisAufraeumen() {
		for (int i = 0; i < this.getLadungsverzeichnis().size(); i++) {
			if (this.getLadungsverzeichnis().get(i).getMenge() == 0) {
				this.getLadungsverzeichnis().remove(i);
			}
		}
	}

}
