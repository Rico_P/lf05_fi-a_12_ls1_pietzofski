package test2;

import java.util.Scanner;

public class Test {

	public static void main(String[] args) {

		int[] lotto = { 3, 7, 12, 18, 37, 42 };

		// A)
		System.out.print("[");
		for (int i : lotto) {
			System.out.printf(" %d ", i);
		}
		System.out.print("]");

		// B)
		Scanner scanner = new Scanner(System.in);

		System.out.println("\nEingabe: ");
		int eingabe = scanner.nextInt();

		boolean check = false;
		for (int i : lotto) {
			if (i == eingabe) {
				check = true;
			}
		}

		if (check) {
			System.out.println("Die Zahl " + eingabe + " ist in der Ziehung entahlten");
		} else {
			System.out.println("Die Zahl " + eingabe + " ist nicht in der Ziehung entahlten");

		}

	}

}