import java.util.Scanner;
import java.lang.Math;

class Fahrkartenautomat {
	public static void main(String[] args) {
		Scanner tastatur = new Scanner(System.in);

		double zuZahlenderBetrag; // Datentyp: Double, da die Zahlung auch mit Cent M�nzen erfolgen kann, sind Rationale Zahleneingaben m�glich
		double eingezahlterGesamtbetrag; // Datentyp: Double, da die Zahlung auch mit Cent M�nzen erfolgen kann, sind Rationale Zahleneingaben m�glich
		double eingeworfeneM�nze; // Datentyp: Double, da die Zahlung auch mit Cent M�nzen erfolgen kann, sind Rationale Zahleneingaben m�glich
		double r�ckgabebetrag; // Datentyp: Double, da die Zahlung auch mit Cent M�nzen erfolgen kann, sind Rationale Zahleneingaben m�glich
		short ticketAnzahl; // Es ist zwar sehr unwahrscheinlich, dass mehr als 127 Tickets gekauft werden, ich habe mich dennoch gegen den Datentyp Byte entschieden. Lieber zu viel als zu wenig. Da der Kauf von "halben" Tickets nicht m�glich sein soll ist die Verwendung von Float oder Double nicht n�tig.
		
		/* Verwendete Operatoren: Vergleichsoperatoren (<,>,>=...) bsp.: Z28, Z53
		 * 						  Rechenoperatoren: Addition, Subtraktion, Multiplikation
		 */

		System.out.print("Zu zahlender Betrag (EURO): ");
		zuZahlenderBetrag = tastatur.nextDouble();
		
		System.out.print("\nAnzahl der Tickets: ");
		ticketAnzahl = tastatur.nextShort();
		
		zuZahlenderBetrag = zuZahlenderBetrag * ticketAnzahl;

		// Geldeinwurf
		// -----------
		eingezahlterGesamtbetrag = 0.0;
		while (eingezahlterGesamtbetrag < zuZahlenderBetrag) {
			//System.out.println("Noch zu zahlen: " + (zuZahlenderBetrag - eingezahlterGesamtbetrag) + " Euro"); // Orginal
			System.out.printf("%s%.2f%s\n", "Noch zu zahlen: ", (zuZahlenderBetrag - eingezahlterGesamtbetrag), " Euro"); // neue Version: Aufgabe 2.4
			System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
			eingeworfeneM�nze = tastatur.nextDouble();
			eingezahlterGesamtbetrag += eingeworfeneM�nze;
		}

		// Fahrscheinausgabe
		// -----------------
		System.out.println("\nFahrschein wird ausgegeben");
		for (int i = 0; i < 8; i++) {
			System.out.print("=");
			try {
				Thread.sleep(250);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.out.println("\n\n");

		// R�ckgeldberechnung und -Ausgabe
		// -------------------------------
		r�ckgabebetrag = (eingezahlterGesamtbetrag - zuZahlenderBetrag);
		if (r�ckgabebetrag > 0.0) {
			// System.out.println("Der R�ckgabebetrag in H�he von " + r�ckgabebetrag + " EURO"); // Original
			System.out.printf("%s%.2f%s\n", "Der R�ckgabebetrag in H�he von ", r�ckgabebetrag, " Euro\n"); // neue Version: nach Aufgabe 2.4
			System.out.println("wird in folgenden M�nzen ausgezahlt:");

			while (r�ckgabebetrag >= 2.0) // 2 EURO-M�nzen
			{
				System.out.println("2 EURO");
				r�ckgabebetrag -= 2.0;
			}
			while (r�ckgabebetrag >= 1.0) // 1 EURO-M�nzen
			{
				System.out.println("1 EURO");
				r�ckgabebetrag -= 1.0;
			}
			while (r�ckgabebetrag >= 0.5) // 50 CENT-M�nzen
			{
				System.out.println("50 CENT");
				r�ckgabebetrag -= 0.5;
			}
			// Werte angepasst um Rundungsfehler, die auf die Gelitkommdarstellung zur�ckzuf�hren sind, zu verhindern.
			while (r�ckgabebetrag >= 0.199) // 20 CENT-M�nzen
			{
				System.out.println("20 CENT");
				r�ckgabebetrag -= 0.2;
			}
			// Werte angepasst um Rundungsfehler, die auf die Gelitkommdarstellung zur�ckzuf�hren sind, zu verhindern.
			while (r�ckgabebetrag >= 0.099) // 10 CENT-M�nzen
			{
				System.out.println("10 CENT");
				r�ckgabebetrag -= 0.1;
			}
			// Werte angepasst um Rundungsfehler, die auf die Gelitkommdarstellung zur�ckzuf�hren sind, zu verhindern.
			while (r�ckgabebetrag >= 0.0499)// 5 CENT-M�nzen
			{
				System.out.println("5 CENT");
				r�ckgabebetrag -= 0.05;
			}
		}

		System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
				+ "Wir w�nschen Ihnen eine gute Fahrt.");
	}
}