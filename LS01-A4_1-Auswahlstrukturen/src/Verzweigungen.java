import java.util.Scanner;

public class Verzweigungen {
	static Scanner scn = new Scanner(System.in);

	public static void main(String[] args) {
		
		double zahl1 = eingabeZahl();
		double zahl2 = eingabeZahl();
		double zahl3 = eingabeZahl();
		
		if (zahl1 > zahl2 && zahl1 > zahl3) {
			System.out.println("Zahl 1 ist gr��er als Zahl 2 und Zahl 3.");
		}
		
		if (zahl3 > zahl2 || zahl3 > zahl1) {
			System.out.println("Zahl 3 ist gr��er als Zahl 2 oder Zahl 1.");
		}
		
		System.out.println("Die gr��te Zahl ist : " + groessteZahl(zahl1, zahl2, zahl3));
		
		// Aufgabe 2: Steuersatz
		System.out.println("Steuersatz");
		System.out.println("----------------------------------");
		System.out.print("Wie hoch ist der Nettopreis? ");
		double nettopreis = scn.nextDouble();
		steuerBerechnung(nettopreis);
		
		// Aufgabe 3: Hardware Gro�h�ndler
		System.out.println("----------------------------------");
		System.out.println("Hardware Gr��h�ndler");
		bestellungMaus();
		
	}

	public static double eingabeZahl() {
		System.out.print("Gebe eine Zahl ein: ");
		double zahl = scn.nextDouble();
		return zahl;
	}
	
	public static double groessteZahl(double a, double b, double c) {
		double grZahl = 0;
		if (a>b && a>c) {
			grZahl = a;
		}
		else if (b>a && b>c) {
			grZahl = b;
		}
		else {
			grZahl = c;
		}
		return grZahl;
	}
	
	public static void steuerBerechnung (double netto) {
		double brutto;
		System.out.print("Erm��igter Steuersatz? j/n ");
		char antwort = scn.next().charAt(0);
		if (antwort == 'j') {
			brutto = netto * 1.07;
			System.out.printf("\n%s%.2f%s\n","Der Bruttopreis ist ",brutto,"�");
		}
		else if (antwort == 'n') {
			brutto = netto * 1.19;
			System.out.printf("\n%s%.2f%s\n","Der Bruttopreis ist ",brutto,"�");
		}
		else {
			System.out.println("Ung�ltige Eingabe");
		}
	}
	
	public static void bestellungMaus() {
		double gesamt;
		double rabatt = 0;
		System.out.print("Wie hoch ist der Einzelpreis der Maus? ");
		double preis = scn.nextDouble();
		System.out.print("Wie viele M�use wollen Sie bestellen? ");
		double anzahl = scn.nextInt();
		
		if (anzahl >= 10) {
			gesamt = (preis*1.19)*anzahl;
		}
		else {
			gesamt = (preis*1.19)*anzahl+10;
		}

		System.out.printf("\n%s%.2f%s\n","Der Bruttogesamtpreis liegt bei: ", gesamt, "�");
		
		if (preis*anzahl > 0 && preis*anzahl <= 100) {
			rabatt = 0.1;
		}
		else if (preis*anzahl > 100 && preis*anzahl <= 500) {
			rabatt = 0.15;
		}
		else {
			rabatt = 0.2;
		}
		
		System.out.println("Sie bekommen einen Rabatt von " + rabatt*100 + "%.");
		
		double gesamtpreis = (gesamt*(1-rabatt));

		System.out.printf("%s%.2f%s","Der Gesamtpreis nach Rabatt und inkl. MwSt liegt bei: ", gesamtpreis,"�");
	}
	
}
