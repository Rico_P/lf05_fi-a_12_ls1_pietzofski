import java.util.Scanner;
import java.lang.Math;

class Fahrkartenautomat_Methoden {
	static Scanner tastatur = new Scanner(System.in);

	public static void main(String[] args) {

		double zuZahlenderBetrag;
		double eingezahlterGesamtbetrag;

		zuZahlenderBetrag = fahrkartenbestellungErfassen();
		eingezahlterGesamtbetrag = fahrkartenBezahlen(zuZahlenderBetrag);
		fahrkartenAusgeben();
		rueckgeldAusgeben(eingezahlterGesamtbetrag, zuZahlenderBetrag);

	}

	public static double fahrkartenbestellungErfassen() {
		double zuZahlenderBetrag = 0;
		int ticketAnzahl = 1;
		int auswahl = 0;
		double zwischensumme = 0;

		String[] karten = new String[10];
		karten[0] = "Einzelfahrschein Berlin AB";
		karten[1] = "Einzelfahrschein Berlin BC";
		karten[2] = "Einzelfahrschein Berlin ABC";
		karten[3] = "Kurzstrecke";
		karten[4] = "Tageskarte Berlin AB";
		karten[5] = "Tageskarte Berlin BC";
		karten[6] = "Tageskarte Berlin ABC";
		karten[7] = "Kleingruppen-Tageskarte Berlin AB";
		karten[8] = "Kleingruppen-Tageskarte Berlin BC";
		karten[9] = "Kleingruppen-Tageskarte Berlin ABC";

		double[] preise = new double[10];
		preise[0] = 2.90;
		preise[1] = 3.30;
		preise[2] = 3.60;
		preise[3] = 1.90;
		preise[4] = 8.60;
		preise[5] = 9;
		preise[6] = 9.60;
		preise[7] = 23.50;
		preise[8] = 24.30;
		preise[9] = 24.90;

		System.out.println("W�hlen Sie Ihre Wunschfahrkarte f�r Berlin aus: ");

		for (int i = 0; i < karten.length; i++) {
			System.out.println(i+1 + ": " + karten[i] + ": " + preise[i] + " �");
		}

		System.out.println();
		System.out.print("Auswahl: ");
		auswahl = tastatur.nextInt();
		System.out.println();
		
		System.out.print("Ticket Anzahl: ");
		ticketAnzahl = tastatur.nextInt();
		System.out.println();

		zwischensumme = preise[auswahl - 1];

		return zwischensumme * ticketAnzahl;
	}

	public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
		double eingezahlterGesamtbetrag;
		double eingeworfeneM�nze;

		eingezahlterGesamtbetrag = 0.0;
		while (eingezahlterGesamtbetrag < zuZahlenderBetrag) {

			System.out.printf("%s%.2f%s\n", "Noch zu zahlen: ", (zuZahlenderBetrag - eingezahlterGesamtbetrag),
					" Euro");
			System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
			eingeworfeneM�nze = tastatur.nextDouble();
			eingezahlterGesamtbetrag += eingeworfeneM�nze;
		}
		return eingezahlterGesamtbetrag;
	}

	public static void fahrkartenAusgeben() {
		System.out.println("\nFahrschein wird ausgegeben");
		for (int i = 0; i < 8; i++) {
			System.out.print("=");
			warte(250);
		}
		System.out.println("\n\n");
	}

	public static void rueckgeldAusgeben(double eingezahlterGesamtbetrag, double zuZahlenderBetrag) {
		// R�ckgeldberechnung und -Ausgabe

		double r�ckgabebetrag = (eingezahlterGesamtbetrag - zuZahlenderBetrag);
		if (r�ckgabebetrag > 0.0) {
			System.out.printf("%s%.2f%s\n", "Der R�ckgabebetrag in H�he von ", r�ckgabebetrag, " Euro\n");

			System.out.println("wird in folgenden M�nzen ausgezahlt:");

			while (r�ckgabebetrag >= 2.0) // 2 EURO-M�nzen
			{
				muenzeAusgeben(2, "EURO");
				r�ckgabebetrag -= 2.0;
			}
			while (r�ckgabebetrag >= 1.0) // 1 EURO-M�nzen
			{
				muenzeAusgeben(1, "EURO");
				r�ckgabebetrag -= 1.0;
			}
			while (r�ckgabebetrag >= 0.5) // 50 CENT-M�nzen
			{
				muenzeAusgeben(50, "CENT");
				r�ckgabebetrag -= 0.5;
			}
			// Werte angepasst um Rundungsfehler, die auf die Gelitkommdarstellung
			// zur�ckzuf�hren sind, zu verhindern.
			while (r�ckgabebetrag >= 0.199) // 20 CENT-M�nzen
			{
				muenzeAusgeben(20, "CENT");
				r�ckgabebetrag -= 0.2;
			}
			// Werte angepasst um Rundungsfehler, die auf die Gelitkommdarstellung
			// zur�ckzuf�hren sind, zu verhindern.
			while (r�ckgabebetrag >= 0.099) // 10 CENT-M�nzen
			{
				muenzeAusgeben(10, "CENT");
				r�ckgabebetrag -= 0.1;
			}
			// Werte angepasst um Rundungsfehler, die auf die Gelitkommdarstellung
			// zur�ckzuf�hren sind, zu verhindern.
			while (r�ckgabebetrag >= 0.0499)// 5 CENT-M�nzen
			{
				muenzeAusgeben(5, "CENT");
				r�ckgabebetrag -= 0.05;
			}
		}

		System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
				+ "Wir w�nschen Ihnen eine gute Fahrt.");
	}

	public static void warte(int mm) {
		try {
			Thread.sleep(mm);
			;
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void muenzeAusgeben(int betrag, String einheit) {
		System.out.println(betrag + " " + einheit);
	}
}
