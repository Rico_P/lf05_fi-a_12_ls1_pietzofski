
public class HelloWorld {

	public static void main(String[] args) {
		// Aufgabe 1
		System.out.println("Aufgabe 1\n");
		
		String s1 = "Das ist ein \"Beispielsatz\".";
		String s2 = "Ein Beispielsatz ist das.";
		
		System.out.printf("%s\n%s", s1, s2);
		
		// Aufgabe 2 
		System.out.println("\n\nAufgabe 2\n");
		
		System.out.printf("%7s\n", "*");
		System.out.printf("%8s\n", "***");
		System.out.printf("%9s\n", "*****");
		System.out.printf("%10s\n", "*******");
		System.out.printf("%11s\n", "*********");
		System.out.printf("%12s\n", "***********");
		System.out.printf("%13s\n", "*************");
		System.out.printf("%8s\n", "***");
		System.out.printf("%8s\n", "***");
		
		// Aufgabe 3 
		System.out.println("\nAufgabe 3\n");
		
		double a = 22.4234234;
		double b = 111.2222;
		double c = 4.0;
		double d = 1000000.551;
		double e = 97.34;
		
		System.out.printf("%.2f\n", a);
		System.out.printf("%.2f\n", b);
		System.out.printf("%.2f\n", c);
		System.out.printf("%.2f\n", d);
		System.out.printf("%.2f\n", e);

		System.out.println("________________________________________________________");
		System.out.println("Gemeinsame �bung\n");
		
		System.out.printf("|Name: %-10s|Age: %-10d|Gewicht: %-10.2f|","Name", 21, 77.7777);
	}	
}