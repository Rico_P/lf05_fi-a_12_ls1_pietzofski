import java.util.Scanner;
public class bmiRechner {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		
		// Eingabe der Werte 
		System.out.print("Gebe dein Geschlecht ein, m oder w: ");
		char gender = scanner.next().charAt(0);
		System.out.print("Gebe dein Gewicht in Kilogramm an: ");
		double weight = scanner.nextDouble();
		System.out.print("Gebe deine Gr��e in cm an: ");
		double height = scanner.nextDouble();
		
		double bmi = bmiBerechnung(weight, height);		
		String klasse = klassifikation(bmi, gender);
		auswertung(bmi,klasse);
	}

	public static double bmiBerechnung (double gewicht, double groesse) {
		// gr��e von cm in m
		groesse = groesse / 100;
		double bmi = (gewicht)/(groesse * groesse);
		return bmi;
	}
	
	public static String klassifikation(double bmi, char gen) {
		String klasse = "Ung�ltig";
		if (gen == 'm') {
			if (bmi < 20) {
				klasse = "Untergewicht";
			}
			else if (bmi >= 20 && bmi <= 25) {
				klasse = "Normalgewicht";
			}
			else {
				klasse = "�bergewicht";
			}
		}
		else if (gen == 'w') {
			if (bmi < 19) {
				klasse = "Untergewicht";
			}
			else if (bmi >= 19 && bmi <= 24) {
				klasse = "Normalgewicht";
			}
			else {
				klasse = "�bergewicht";
			}
		}
		
		return klasse;
	}
	
	public static void auswertung (double bmi, String klassifikation) {
		System.out.printf("%s%.2f%s%s.","Dein BMI liegt bei: ", bmi, ", das bedeutet du hast ", klassifikation);
	}
}
